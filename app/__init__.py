from flask import Flask
from flask_cors import CORS
from config import Config
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
CORS(app, origins="*", allow_headers=[
    "Content-Type",
    "Authorization",
    "Access-Control-Allow-Credentials"
], supports_credential=True)
app.config.from_object(Config)
db = SQLAlchemy(app)

from app.routes import complaint, user, history

