from flask import jsonify, request, abort

from app import app, db
from app.models.history import History

@app.route("/histories", methods=["POST"])
def add_new_history():
    if not request.json:
        abort(400, "Request should be in JSON format")
    if 'id' not in request.json or 'name' not in request.json:
        abort(400, "One or more required fields not found")

    new_history = History(
        id=request.json["id"],
        category=request.json["category"],
        risk=request.json["risk"],
        name=request.json["name"],
        phone=request.json["phone"],
        email=request.json["email"],
        description=request.json["description"],
        status=request.json["status"],
        cs_id=request.json["cs_id"],
        registered_datetime=request.json["registered_datetime"]
    )

    db.session.add(new_history)
    db.session.commit()

    return jsonify(new_history.to_json()), 201

@app.route("/histories", methods=["GET"])
def get_all_histories():

    histories = [
        history.to_json()
        for history in History.query.all()
    ]

    return jsonify(histories)

@app.route("/histories/<string:category>", methods=["GET"])
def get_category_history(category: str):
    categoryHistory = [
        getHistory.to_json()
        for getHistory in History.query.filter(History.category.endswith(category)).all()
    ]

    return jsonify(categoryHistory)

@app.route("/histories", methods=["DELETE"])
def delete_histories():
    db.session.query(History).delete()
    db.session.commit()

    return jsonify({
        "result": "success"
    })


