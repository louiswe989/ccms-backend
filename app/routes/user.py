from flask import jsonify, request, abort
from app import app, db
from app.models.user import User

@app.route("/users", methods=["GET"])
def get_all_users():

    positions = ['Senior CS', 'Junior CS']

    users = [
        user.to_json()
        for user in User.query.filter(User.position.in_(positions)).all()
    ]

    return jsonify(users)


@app.route("/users/<string:token>", methods=["GET"])
def get_specific_user(token: str):
    user = User.verify_auth_token(token)

    if user is None:
        abort(404, "user not found")

    return jsonify(user.to_json())


@app.route("/users", methods=["POST"])
def add_new_user():
    if not request.json:
        abort(400, "Request should be in JSON format")
    if 'id' not in request.json or 'name' not in request.json:
        abort(400, "One or more required fields not found")
    
    existing_user = User.query.filter_by(id=request.json["id"]).first()

    if existing_user is not None:
        abort(409, "User already exist")

    new_user = User(
        id=request.json["id"],
        password="",
        name=request.json["name"],
        position=request.json["position"],
        phone=request.json["phone"],
        email=request.json["email"]
    )

    new_user.hash_password(request.json["password"])

    db.session.add(new_user)
    db.session.commit()

    return jsonify(new_user.to_json()), 201

@app.route("/login", methods=["POST"])
def loginValidation():
    id = request.json["id"]
    password = request.json["password"]
    user = User.query.filter_by(id = id).first()
    if not user or not user.verify_password(password):
        return jsonify({"result":"failed"}),404

    token = user.generate_auth_token()
    return jsonify({'token': token.decode('ascii')})

@app.route("/users/<string:token>", methods=["PUT"])
def update_user(token: str):
    existing_user = User.verify_auth_token(token)

    if existing_user is None:
        abort(404, "Request should be in JSON format")
    if not request.json:
        abort(400, "Request should be in JSON format")
    if 'name' not in request.json:
        abort(400, "One or more required fields not found")
    if type(request.json['name']) != str:
        abort(400, "One or more fields does not match the required format")

    existing_user.name = request.json["name"]
    existing_user.phone = request.json["phone"]
    existing_user.hash_password(request.json["password"])

    db.session.add(existing_user)
    db.session.commit()

    return jsonify(existing_user.to_json())


@app.route("/users/<string:id>", methods=["DELETE"])
def delete_user(id: str):
    existing_user = User.query.filter_by(id=id).first()

    if existing_user is None:
        abort(404, "user not found")

    db.session.delete(existing_user)
    db.session.commit()

    return jsonify({
        "result": "success"
    })