from flask import jsonify, request, abort

from app import app, db
from app.models.complaint import CustomerComplaint

@app.route("/complaints", methods=["GET"])
def get_all_complaints():

    complaints = [
        complaint.to_json()
        for complaint in CustomerComplaint.query.all()
    ]

    return jsonify(complaints)

@app.route("/complaints/<string:category>", methods=["GET"])
def get_category_complaint(category: str):
    categoryComplaint = [
        getCategory.to_json()
        for getCategory in CustomerComplaint.query.filter(CustomerComplaint.category.endswith(category)).all()
    ]

    return jsonify(categoryComplaint)

@app.route("/complaints", methods=["POST"])
def add_new_complaint():
    if not request.json:
        abort(400, "Request should be in JSON format")
    if 'id' not in request.json or 'name' not in request.json:
        abort(400, "One or more required fields not found")

    new_complaint = CustomerComplaint(
        category=request.json["category"],
        risk=request.json["risk"],
        name=request.json["name"],
        phone=request.json["phone"],
        email=request.json["email"],
        description=request.json["description"],
        status=request.json["status"],
        cs_id=request.json["cs_id"],
        registered_datetime=request.json["registered_datetime"]
    )

    db.session.add(new_complaint)
    db.session.commit()

    return jsonify(new_complaint.to_json()), 201

@app.route("/complaints/<string:id>", methods=["PUT"])
def update_complaint(id: str):
    existing_complaint = CustomerComplaint.query.filter_by(id=id).first()

    if existing_complaint is None:
        abort(404, "Request should be in JSON format")
    if not request.json:
        abort(400, "Request should be in JSON format")
    if 'name' and 'phone' and 'description' not in request.json:
        abort(400, "One or more required fields not found")
    if type(request.json['name'] and request.json['phone'] and request.json['description']) != str:
        abort(400, "One or more fields does not match the required format")

    existing_complaint.name = request.json["name"]
    existing_complaint.phone = request.json["phone"]
    existing_complaint.description = request.json["description"]

    db.session.add(existing_complaint)
    db.session.commit()

    return jsonify(existing_complaint.to_json())

@app.route("/complaints/<string:id>", methods=["DELETE"])
def delete_complaint(id: str):
    existing_complaint = CustomerComplaint.query.filter_by(id=id).first()

    if existing_complaint is None:
        abort(404, "user not found")

    db.session.delete(existing_complaint)
    db.session.commit()

    return jsonify({
        "result": "success"
    })

