# from app import db
#
# class Post(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     author_id = db.Column(db.String(128), db.ForeignKey('user.id'))
#     message = db.Column(db.String(1024))
#
#     def __repr__(self):
#         return "<Post {}>".format(self.id)
#
#     def to_json(self):
#         return {
#             "id": self.id,
#             "author_id": self.author_id,
#             "message": self.message
#         }

from app import db

class History(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String(128))
    risk = db.Column(db.Integer)
    name = db.Column(db.String(128))
    phone = db.Column(db.String(128))
    email = db.Column(db.String(128))
    description = db.Column(db.String(1024))
    status = db.Column(db.String(128))
    cs_id = db.Column(db.String(128))
    registered_datetime = db.Column(db.String(128))

    def __repr__(self):
        return "<History {}>".format(self.id)

    def to_json(self):
        return {
            "id": self.id,
            "category": self.category,
            "risk": self.risk,
            "name": self.name,
            "phone": self.phone,
            "email": self.email,
            "description": self.description,
            "status": self.status,
            "cs_id": self.cs_id,
            "registered_datetime": self.registered_datetime
        }