from app import db
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from config import Config
from passlib.apps import custom_app_context as pwd_context

class User(db.Model):
    id = db.Column(db.String(128), primary_key=True)
    password = db.Column(db.String(128))
    name = db.Column(db.String(256))
    position = db.Column(db.String(256))
    phone = db.Column(db.String(256))
    email = db.Column(db.String(256))

    complaints = db.relationship("CustomerComplaint", backref="author", lazy="dynamic")

    def __repr__(self):
        return '<User {}>'.format(self.id)

    def to_json(self):
        return {
            "id": self.id,
            "password": self.password,
            "name": self.name,
            "position": self.position,
            "phone": self.phone,
            "email": self.email
        }

    def generate_auth_token(self, expiration = 86400):
        s = Serializer(Config.SECRET_KEY, expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(Config.SECRET_KEY)
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None
        except BadSignature:
            return None
        user = User.query.get(data['id'])
        return user

    def hash_password(self, password):
        self.password = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password)
