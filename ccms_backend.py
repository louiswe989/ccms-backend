from app import app, db
from app.models.complaint import CustomerComplaint
from app.models.user import User
from app.models.history import History

@app.shell_context_processor
def make_shell_context():
    return {
        "db": db,
        "CustomerComplaint": CustomerComplaint,
        "User": User,
        "History": History
    }